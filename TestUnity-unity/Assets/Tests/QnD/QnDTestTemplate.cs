using UnityEngine;
using System.Collections;
using NUnit.Framework;
using abutt1.Tests;

[QnDTestFixture]
public class QnDTestTemplate
{	
	#region fields
	#endregion
	
	
	[QnDSetup]
	public void SetupTest()
	{		
		RegisterListeners();
	}
		
	[QnDTearDown]
	public void TearDownTest()
	{
		UnregisterListeners();
	}
	
	[QnDTest]
	public void ExampleTest()
	{
		string              testMessage = "0: I am an example test";
		
		// check all our filepaths are valid/invalid
		Assert.That(true,                                   Is.EqualTo(true),                                   string.Format("Test:{0}: {1}", testMessage, "a"));
	}
	
	[QnDTest]
	public IEnumerator ExampleTest2()
	{
		string              testMessage = "0: I am an example Enumerator test";
		
		int counter = 0;
		while(++counter < 5) { yield return false; }
		
		Assert.That(counter,                                Is.EqualTo(5),                                      string.Format("Test:{0}: {1}", testMessage, "a"));
	}
	
	#region listeners

	void RegisterListeners()
	{
	}

	void UnregisterListeners()
	{
	}
	
	#endregion
	
}

