using UnityEngine;
using System.Collections;
using NUnit.Framework;
using abutt1.Tests;

[QnDTestFixture]
[TestFixture]
public class MergeTest
{	
	#region fields
	bool PassTest1 { get { return true; } }
	bool PassTest2 { get { return false; } }
	bool PassTest3 { get { return true; } }
	
	bool DoDebug   { get { return false; } }
	
	#endregion
	
	[QnDSetup]
	[SetUp]
	public void SetupTest()
	{
		if(DoDebug) { Debug.Log("SetupTest"); }
		RegisterListeners();
	}
	
	[QnDTearDown]
	[TearDown]
	public void TearDownTest()
	{
		if(DoDebug) { Debug.Log("TearDownTest"); }
		UnregisterListeners();
	}
	
	[QnDTest]
	[Test]
	public void ExampleTest_1()
	{
		if(DoDebug) { Debug.LogWarning("ExampleTest_1"); }
		string              testMessage = "Test1";
		
		Assert.That(PassTest1,                              Is.EqualTo(true),                                   string.Format("Test:{0}: {1}", testMessage, "a"));
	}
	
	[QnDTest]
	[Test]
	public void ExampleTest_2()
	{
		if(DoDebug) { Debug.LogWarning("ExampleTest_2"); }
		string              testMessage = "Test2";
		
		Assert.That(PassTest2,                              Is.EqualTo(true),                                   string.Format("Test:{0}: {1}", testMessage, "a"));
	}
	
	[QnDTest]
	[Test]
	public void ExampleTest_3()
	{
		if(DoDebug) { Debug.LogWarning("ExampleTest_3"); }
		string              testMessage = "Test3";
		
		Assert.That(PassTest3,                              Is.EqualTo(true),                                   string.Format("Test:{0}: {1}", testMessage, "a"));
	}	
	
	#region listeners

	void RegisterListeners()
	{
	}

	void UnregisterListeners()
	{
	}
	
	#endregion
	
}

