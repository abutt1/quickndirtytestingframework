using UnityEngine;
using System.Collections;
using NUnit.Framework;

[TestFixture]
public class NUnitTestTemplate
{	
	#region fields
	#endregion
	
	
	[SetUp]
	public void SetupTest()
	{		
		RegisterListeners();
	}
		
	[TearDown]
	public void TearDownTest()
	{
		UnregisterListeners();
	}
	
	[Test]
	public void ExampleTest()
	{
		string              testMessage = "0: I am an example test";
		
		// check all our filepaths are valid/invalid
		Assert.That(true,                                   Is.EqualTo(true),                                   string.Format("Test:{0}: {1}", testMessage, "a"));
	}
	
	#region listeners

	void RegisterListeners()
	{
	}

	void UnregisterListeners()
	{
	}
	
	#endregion
	
}

