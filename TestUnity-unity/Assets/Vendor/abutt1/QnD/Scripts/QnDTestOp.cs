﻿using System;
using System.Collections;
using System.Reflection;
using abutt1;

namespace abutt1.Tests
{
	public interface IQnDTestOp : ICompletionChecker<IQnDTestOp>
	{
		int         TestsPerformed { get; }
		int         TestsPassed    { get; }
		int         TestsFailed    { get; }
	}
	
	public class QnDTestOp : SimpleOperation<IQnDTestOp>, IQnDTestOp {
		
		public int  TestsPerformed { get { return testsPerformed;                   } }
		public int  TestsPassed    { get { return testsPerformed - testsFailed;     } }
		public int  TestsFailed    { get { return testsFailed;                      } }
		
		int         testsPerformed;
		int         testsFailed;
		
		public QnDTestOp(Type typeToTest)
		{
			curOp = TestType(typeToTest);
		}
		
		IEnumerator TestType(Type toTest)
		{
			// TODO: take into account fixtures, to replicate NUnit's stuff
			
			MethodInfo      setupMethod     = QnDTestHelper.GetSetupMethod(toTest);
			MethodInfo      tearDownMethod  = QnDTestHelper.GetTearDownMethod(toTest);
			
			MethodInfo[]    testMethods     = QnDTestHelper.GetTestMethods(toTest);
			
			string          testErrorStr    = "";
			
			// TODO: some sort of enumerator based on TestFixtures
			{
				object testObj = Activator.CreateInstance(toTest); // + fixture params if any....
				for (int i = 0; i < testMethods.Length; i++) {
					
					MethodInfo curTestMethod = testMethods[i];
					
					IEnumerator testOp = TestMethod(testObj, curTestMethod, setupMethod, tearDownMethod);
					
					bool finished = false;
					while (!finished) {
						try {
							finished = !testOp.MoveNext();
						} catch (Exception ex) {
							
							string errorBaseStr = string.Format("{0} ({1}.{0})\n", curTestMethod.Name, curTestMethod.DeclaringType.Name);
							
							testErrorStr += errorBaseStr + ex.Message + "\n\n";
							
							testsFailed++;
							
							break;
						}
						
						if(!finished) { yield return false; }
					}
					
					testsPerformed++;
				}
				
			}
			
			error = testErrorStr;
		}
		
		static IEnumerator TestMethod(object testObj, MethodInfo testMethod, MethodInfo setupMethod, MethodInfo tearDownMethod)
		{
			if(setupMethod != null)
			{
				IEnumerator methodOp = InvokeMethodEnumerator(testObj, setupMethod, "Failed setup method\n");
				while(methodOp.MoveNext()) { yield return false; }
			}
			
			System.Exception exToThrow = null;
			// if an exception occurs in the testMethod, we still should teardown
			if(testMethod != null)
			{
				IEnumerator methodOp = InvokeMethodEnumerator(testObj, testMethod, "");
				
				bool finished = false;
				while(!finished)
				{
					try {
						finished = !methodOp.MoveNext();
					} catch (Exception ex) {
						exToThrow = ex;
						finished  = false;
					}
					if(!finished) { yield return false; }
				}
			}
			
			if(tearDownMethod != null)
			{
				IEnumerator methodOp = InvokeMethodEnumerator(testObj, tearDownMethod, "Failed teardown method\n");
				while(methodOp.MoveNext()) { yield return false; }				
			}
			
			if(exToThrow != null)
			{
				throw exToThrow;
			}
		}
		
		static IEnumerator InvokeMethodEnumerator(object onObj, MethodInfo method, string errorBaseMessage)
		{
			object methodCallObj = method.IsStatic ? null : onObj;
			
			object retVal        = null;
			
			try {
				retVal = method.Invoke(methodCallObj, null);
			} catch (TargetInvocationException ex) {
				throw new System.Exception( errorBaseMessage + ex.InnerException.Message);
			} catch (Exception ex) {
				throw new System.Exception( errorBaseMessage + ex.Message );
			}
			
			if(retVal != null)
			{
				// if it is an enumerator... do it!
				if( typeof(IEnumerator).IsAssignableFrom(retVal.GetType()) )
				{
					bool finished = false;
					IEnumerator methodOp = (IEnumerator)retVal;
					while (!finished) {
						try {
							finished = !methodOp.MoveNext();
						} catch (Exception ex) {
							throw new System.Exception( errorBaseMessage + ex.Message );
						}
						
						if(!finished) { yield return false; }
					}
				}
			}
		}
		
	}
}
