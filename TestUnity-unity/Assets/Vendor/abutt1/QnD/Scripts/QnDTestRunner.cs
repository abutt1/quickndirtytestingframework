﻿using UnityEngine;
using System;
using System.Collections;
using System.Reflection;

namespace abutt1.Tests
{
	public class QnDTestRunner : MonoBehaviour 
	{
		public static void RunTests(Type[] types)
		{
			if(types == null || types.Length == 0) { return; }
			
			QnDTestRunner runner = new GameObject("QnDTestRunner").AddComponent<QnDTestRunner>();
			
			runner.StartCoroutine(TestTypes(types));
		}
		
		static IEnumerator TestTypes(Type[] toTest)
		{
			int totalPerformed = 0;
			int totalFailed    = 0;
			string errorStr    = "";
			
			for (int i = 0; i < toTest.Length; i++) {
				
				QnDTestOp curTest = new QnDTestOp(toTest[i]);

				while(curTest.MoveNext()) { yield return false; }
				
				totalPerformed += curTest.TestsPerformed;
				totalFailed    += curTest.TestsFailed;
				
				errorStr       += curTest.Error;
			}
			
			string debugStr = string.Format( "Adrian's Quick 'n Dirty Test Framework: {0} Ran, {1} Failed\n", totalPerformed, totalFailed);
			
			if(totalFailed > 0)
			{
				debugStr += "\nFailures:\n\n" + errorStr;
				
				Debug.LogWarning(debugStr);
			}
			else
			{
				Debug.Log(debugStr);
			}
			
		}
	}
}
