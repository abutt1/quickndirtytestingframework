﻿using UnityEngine;
using System.Collections;

namespace abutt1.Tests
{
	[System.AttributeUsage (System.AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
	public sealed class QnDTestFixtureAttribute : System.Attribute
	{
		
	}
	
	
	[System.AttributeUsage (System.AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public sealed class QnDTestAttribute : System.Attribute
	{
		
	}

	[System.AttributeUsage (System.AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
	public sealed class QnDSetupAttribute : System.Attribute
	{
		
	}

	[System.AttributeUsage (System.AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
	public sealed class QnDTearDownAttribute : System.Attribute
	{
		
	}
	
}
