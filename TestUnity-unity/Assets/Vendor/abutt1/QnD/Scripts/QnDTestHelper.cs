﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
namespace abutt1.Tests
{
	public class QnDTestHelper {
		
		public static MethodInfo GetTearDownMethod(Type fromType)
		{
			MethodInfo[] methods = GetMethodsFromTypeWithAttribute(fromType, typeof(QnDTearDownAttribute));
			
			if(methods.Length > 0)
			{
				return methods[0];
			}
			
			return null;
		}
		
		public static MethodInfo GetSetupMethod(Type fromType)
		{
			MethodInfo[] methods = GetMethodsFromTypeWithAttribute(fromType, typeof(QnDSetupAttribute));
			
			if(methods.Length > 0)
			{
				return methods[0];
			}
			
			return null;
		}

		public static MethodInfo[] GetTestMethods(Type fromType)
		{
			return GetMethodsFromTypeWithAttribute(fromType, typeof(QnDTestAttribute));
		}
		
		
		private static MethodInfo[] GetMethodsFromType(Type fromType)
		{
			return fromType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
		}
		
		private static MethodInfo[] GetMethodsFromTypeWithAttribute(Type fromType, Type withAttributeType)
		{
			List<MethodInfo> methods = new List<MethodInfo>(GetMethodsFromType(fromType));
			
			// attempt to find any matching attributes if we have to
			if(withAttributeType != null)
			{
				for (int i = 0; i < methods.Count; i++) {
					
					object[] attributes = methods[i].GetCustomAttributes(withAttributeType, false);
					
					if(attributes.Length == 0)
					{
						methods.RemoveAt(i--);
					}
				}
			}
			
			return methods.ToArray();
		}
	}
}
