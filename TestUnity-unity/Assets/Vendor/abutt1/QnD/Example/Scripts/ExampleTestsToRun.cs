﻿using UnityEngine;
using System;
using System.Collections;
using abutt1.Tests;

public class ExampleTestsToRun : MonoBehaviour {

	public bool DoNUnit;
	public bool DoQnD;
	
	// Use this for initialization
	void Start () {
		
		Type[] nunitTests = new Type[] {
			typeof(MergeTest),
		};
		Type[] qndTests = new Type[] {
			typeof(QnDTestTemplate),
			typeof(MergeTest),
		};
	
		if(DoNUnit) { EENUnitLiteUnityRunner.RunTests(nunitTests); }
		if(DoQnD)   { QnDTestRunner.RunTests(qndTests);            }
		
	}
	
}
