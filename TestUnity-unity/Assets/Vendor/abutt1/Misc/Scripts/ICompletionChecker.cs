﻿using System.Collections;

namespace abutt1
{
	public delegate void ICompletionCheckerDelegate<T>(T completedOp);
	public interface ICompletionChecker<T>
	{
		event   ICompletionCheckerDelegate<T> OperationFinishedEvent;
		bool    IsDone      { get; }
		float   Progress    { get; }
		string  Error       { get; }
	}
	
	public interface ICompletionChecker : ICompletionChecker<ICompletionChecker> {}
	
	public abstract class SimpleOperation<T> : IEnumerator, ICompletionChecker<T>
	{
		public event ICompletionCheckerDelegate<T> OperationFinishedEvent;
		
		protected   IEnumerator curOp;
		protected   string      error;
		protected   float       curProgress;
		
		public      bool        IsDone      { get { return (curOp == null);         } }
		public      float       Progress    { get { return GetCalculatedProgress(); } }
		public      string      Error       { get { return error;                   } }
		
		public SimpleOperation()                        : this(null) { }
		public SimpleOperation(IEnumerator toPerform)
		{
			curOp = toPerform != null ? toPerform : NullRefEnumerator();
		}
		
		#region IEnumerator
		
		public bool MoveNext()
		{
			if(curOp != null)
			{
				try 
				{
					if(curOp.MoveNext())
					{
						return true;
					}
				} catch (System.Exception ex) {
					error = ex.Message;
				}
				
				if(OperationFinishedEvent != null)
				{
					OperationFinishedEvent(RelevantObject);
				}
				
				curOp = null;
			}
			return false;
		}
		
		public virtual object Current 
		{
			get 
			{
				if(curOp != null)
				{
					return curOp.Current;
				}
				return null;
			}
		}
		
		public virtual void Reset ()
		{
			throw new System.NotImplementedException ();
		}
		
		#endregion
			
		protected virtual float GetCalculatedProgress()
		{
			if(IsDone)
			{
				return string.IsNullOrEmpty(Error) ? 1 : 0;
			}
			return curProgress;
		}
		
		protected virtual T RelevantObject
		{
			get
			{
				if(typeof(T).IsAssignableFrom(this.GetType()))
				{
					return (T)(object)this;
				}
				return default(T);
			}
		}
		
		IEnumerator NullRefEnumerator() { yield return false; throw new System.Exception("It appears as though an enumerator wasn't provided."); }
	}	
}
